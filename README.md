# zram-simple

Tested in Ubuntu 18.04.1, Ubuntu 20.04.1.

## To install

```
sudo env ZRAM_SIZE=4GB make install
sudo systemctl enable zram-simple
sudo systemctl start zram-simple
```

## To uninstall

```
sudo make uninstall
```

## License

MIT.
