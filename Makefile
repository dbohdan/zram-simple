BIN_DIR ?= /usr/local/bin
SYSTEMD_DIR ?= /lib/systemd/system
ZRAM_PRIORITY ?= 1000
ZRAM_SIZE ?= 4GB

install:
	install -m 0755 zram-swap-start $(BIN_DIR)
	install -m 0755 zram-swap-stop $(BIN_DIR)
	sed -i 's/ZRAM_PRIORITY/$(ZRAM_PRIORITY)/' $(BIN_DIR)/zram-swap-start
	sed -i 's/ZRAM_SIZE/$(ZRAM_SIZE)/' $(BIN_DIR)/zram-swap-start
	cp zram-simple.service $(SYSTEMD_DIR)/zram-simple.service

uninstall:
	-systemctl stop zram-simple
	-systemctl disable zram-simple
	-rm $(BIN_DIR)/zram-swap-start
	-rm $(BIN_DIR)/zram-swap-stop
	-rm $(SYSTEMD_DIR)/zram-simple.service

inspect:
	@printf '$$ '
	-cat $(BIN_DIR)/zram-swap-start
	@printf '======\n$$ '
	-cat $(BIN_DIR)/zram-swap-stop
	@printf '======\n$$ '
	-cat $(SYSTEMD_DIR)/zram-simple.service

.PHONY: inspect install uninstall
